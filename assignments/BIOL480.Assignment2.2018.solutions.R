# Part A, Q1

nchar(walden)
# 666711

# Q2.
substr(walden, 997, 1010)
# "\nHigher Laws\r\n"

# Q3
# \n is a new line. \r is carriage return. 
# a new line goes "down" one line. a carriage return puts the cursor 
# at the beginning of the line.
# on a mac, \n is equal to \n\r (it does both)

# Q4
substr(walden, 840, 1150) # close enough

# Q5
regexpr(text = walden, 
        pattern = "\n=WALDEN=\r\n\r\nEconomy\r\n\r\nWhere I Lived,")[1]

# Q6
pat <- "but not yet anywhere seen."
tmp <- regexpr(text = walden, pattern = pat)[1]
( tmp + nchar(pat) - 1)
(substr(walden, ( tmp + nchar(pat) - 1), ( tmp + nchar(pat) - 1)))

#Q7
walden.txt <- strsplit(walden, 
                       split = "\n=WALDEN=\r\n\r\nEconomy\r\n\r\nWhere I Lived," )[[1]][2]
# well this actually cuts off the title and the first two chapter names but this is fine.

walden.txt <- strsplit(walden.txt, split = "but not yet anywhere seen.")[[1]][1]
print(walden.txt)
# man, it would by annoying if you bought a copy of the book and it ended with a comma
walden.txt <- paste(walden.txt, "but not yet anywhere seen.")


# Q8
insight.ind <- regexpr( pattern = "insight", text = walden.txt )
cat("\n Walden before: ", substring(walden.txt, insight.ind[1]-20, insight.ind[1]+20 ))
new.walden.txt <- sub(pattern = "insight", replacement = "money", x = walden.txt )
cat("\n Walden after: ", substring(new.walden.txt, insight.ind[1]-20, insight.ind[1]+20 ))

# Q9
forest.occurrences <- as.list(
              gregexpr( pattern = "forest", 
                        text = walden.txt, 
                        ignore.case = TRUE) ) 

# Q 10
trumps.walden <- gsub( pattern= "forest", 
                            replacement = "concrete jungle",
                            x = walden.txt,
                            ignore.case = TRUE )
cat("Real Walden: ", substring(walden.txt, 591455-50, 591455+50))
cat("Idiot's Walden: ", substring(trumps.walden, 591455+200, 591455+400))


#Part B
# Question 1

genetic.code <- c( "Ala","Arg", "Asn",
                   "Asp", "Cyc", "Gln",
                   "Glu", "Gly", "His",
                   "Ile", "Leu", "Lys",
                   "Met", "Phe", "Pro",
                   "Ser", "Thr", "Trp",
                   "Tyr", "Val")

# Question 2
genetic.code <- c( GCT = "Ala", GCC = "Ala", GCA = "Ala", GCG = "Ala"
                   ### etc. etc. for all 20 amino acidss
                   )

# Question 3

AminoAcid2Codons <- function( AminoAcid ) {
  codons <- list()
  for (i in 1:length(genetic.code)) {
    if (genetic.code[i] == AminoAcid) {
      codons[length(codons)+1] <- names(genetic.code)[i]
    } # end of it
  }# end of for
  return(codons)
}

# Question 4 

WatsonCrick <- function( InputStrand ) {

  RevStrand <- ""
  for (i in nchar(InputStrand):1) {
        current <- substr(InputStrand, i, i)
        
        if (current == "A")  wc <- "T"
        else if (current == "T") wc <- "A"
        else if (current == "G") wc <- "C"
        else if (current == "C") wc <- "G"

        RevStrand <- paste( RevStrand, wc, sep = "")
  } # end of for
  return(RevStrand)
} # end of wC



# Question 5


# there are many ways to solve this. i've chosen a solution
# here that uses the simplest R commands/functions and tried
# to make it as transparent/easy to read as possible.


WatsonCrick <- function( InputStrand ) {
  
  # to allow the user to enter either in lower  or upper,
  # let's put it all in lower
  InputStrand <- tolower(InputStrand)

  RevStrand <- ""
  for (i in nchar(InputStrand):1) {
    current <- substr(InputStrand, i, i)
    
    if (current == "a")  wc <- "t"
    else if (current == "t") wc <- "a"
    else if (current == "c") wc <- "g"
    else if (current == "g") wc <- "c"
    
    RevStrand <- paste( RevStrand, wc, sep = "")
  } # end of for
  
  return(RevStrand)
} # end of wC

# Question 6

# there are many ways to solve this. i've chosen a solution
# here that uses the simplest R commands/functions and tried
# to make it as transparent/easy to read as possible.

Nuc2AA <- function( seq ) {
  
  if (nchar(seq) %% 3 != 0 ) {
    stop("The length of the sequence is not modulus 3.")
  }
  
  for (i in 1:nchar(seq)) {
    if (!(substring(seq, i, i) %in% c("A", "C", "G", "T"))) {
      stop("woops non-nucleotide")
    }
  } # end of for
  
  aa.seq <- character()
  for (i in 0:(nchar(seq)/3 -1)) {
    current <- substring(seq, 3*i+1, 3*(i+1))

        for (j in 1:length(names(genetic.code))) {
      if (names(genetic.code[j]) == current) {
        aa.seq[j] <- genetic.code[j]
      } # if
    } # for j
  } # end i
  return(aa.seq)
} # end of Nuc2AA


# Part C

# Question 1
class(chrYgenes)

# Question 2 (the first 2 :-)
chrYgenes[[12]]$hgnc_symbol
chrYgenes[[12]]$entrez
# https://www.google.ca/search?q=entrez+378948&oq=entrez+378948&aqs=chrome..69i57.14415j0j7&sourceid=chrome&ie=UTF-8

# To be honest, I think I was trying assk what type of binding domain does it have 
# (answer: N-terminus RNA binding domain)
# to me it looks pretty typical.
# this is the link to the PFAM (Protein family database) for the binding domain:
# https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=285341

# Question 2 (the second 2)
length(chrYgenes)

# Question 3

tot <- 0
for (i in 1:length(chrYgenes)) {
  if (!is.na(chrYgenes[[i]]$hgnc_symbol) && is.na(chrYgenes[[i]]$entrez)) {
      tot <- tot + 1
  }
}
print(tot)

# I am actually quite surprised that so many HGNC named genes do not have entrez IDs. 
# I suspect that there are in fact entries in the Entrez gene database at the NCBI
# but the link between the HGNC and the entrez ID hasn't been filled in. That's weird.
# I am interested to see if anyone else has suggestions why so many are missing here.

# Question 4

tot.promoter <- tot.protein <- 0
for (i in 1:length(chrYgenes)) {
  if (chrYgenes[[i]]$promoter != "Sequence unavailable")  {
    tot.promoter <- tot.promoter + 1
  }
  if (chrYgenes[[i]]$protein != "Sequence unavailable")  {
    tot.protein <- tot.protein + 1
  }
}

print(tot.promoter)
print(tot.protein)

# Question 6

to.be.removed <- c()
for (i in 1:length(chrYgenes)) {
  if ((chrYgenes[[i]]$promoter == "Sequence unavailable") ||
      (chrYgenes[[i]]$protein == "Sequence unavailable") ||
      (is.na(chrYgenes[[i]]$hgnc_symbol)) ||
      (is.na(chrYgenes[[i]]$entrez))){
    to.be.removed <- c(to.be.removed, i)
  }
}
well.annotated.genes <- chrYgenes[-to.be.removed]

# Question 7

tot.W <- 0
for (i in 1:length(chrYgenes)) {
  if (chrYgenes[[i]]$protein != "Sequence unavailable") {
      inds <- gregexpr( pattern = "W", text = chrYgenes[[i]]$protein )[[1]]
      if (length(inds) > 1) 
        tot.W <- tot.W + 1
      }
}

cat( tot.W / length(chrYgenes) )

# Question 8

inds.g <- 0
total.length <- 0

for (i in 1:length(chrYgenes)) {
  if (chrYgenes[[i]]$promoter != "Sequence unavailable") {
    total.length <- total.length + nchar(chrYgenes[[i]]$promoter)
    inds.g <- inds.g + 
                length(gregexpr( pattern = "G", text = chrYgenes[[i]]$promoter )[[1]]) +
                length(gregexpr( pattern = "C", text = chrYgenes[[i]]$promoter )[[1]])
  }
}
cat(inds.g/total.length)

