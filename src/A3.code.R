diff.genes<-table(transcripts$gene.name)

gene.exprs <- scale( t( simplify2array( lapply( names(diff.genes), 
                      FUN = function(x) {
                        
                        ind <- which(transcripts == x)
                        if (length(ind) == 1) return(exprs[ind,]) else
                          return( apply( exprs[ind,], MARGIN=2, sum ) )
                      }) )) )

rownames(gene.exprs) <- names(diff.genes)


      
my.data <- list( exprs = gene.exprs, clinical = clinical)

save(my.data, file = "~/repos/T4LS18/data/my.data.RData")
