# Below I present hierarchical clustering in R.
# Although there are existing libraries and functions for many of the steps
# I have done everything from first principles here in order to give
# a (hopefully) more informative explanation of how this works, and to help
# advance your programming skills.

# The input is usually an expression matrix. Here is a simple one:
sample.names <- c("sample1", "sample2", "sample3", "sample4")
gene.names <- c("gene1", "gene2" )
X <- matrix( nrow = length(gene.names), ncol = length(sample.names), 
             dimnames = list( 
               genes = gene.names, 
               samples = sample.names),
             data = c(1, 2, 4, 9, 3, 7, 11, 6))

# Next we need a distance function. There are many possibilities but let's use
# Euclidean distance (based on the Pythogorean theorem).
# here we take two numeric vectors (with an arbitrary but equal number of dimensions ie length())
# and return the distance

Euclidean.distance <- function(x, y) {
  
  if (length(x) != length(y)) { stop("What are you doing?!?")}
  
  return( sqrt( sum((y-x)^2 )) )
}

# Example: let's compute the distance between our two samples
print( Euclidean.distance( X[,"sample1"], X[,"sample2"]) )

# Now we need to "convert" our expression matrix X into a distance matrix D
# D is a symmetric matrix where D[i,j] is the distance between sample i and j.
# (It's symmetric because the distance from Montreal to Toronto is the same as Toronto to Montreal.)

# Step 0: Compute the distance matrix.

D <- matrix( nrow = length(sample.names), ncol = length(sample.names), 
             dimnames = list( 
               samples = sample.names,
                samples = sample.names),
             data = 0)

for (i in 1:(ncol(X)-1)) {  # for every sample (except the last one)
  for (j in (i+1):ncol(X)) {  # for every sample labelled higher than i (including the last one)
    D[i,j] <- D[j,i] <- Euclidean.distance( X[,i], X[,j])
  }
}

print(D)
# To make things simpler to find the minimum in the D during our algorithm below,
# we modify D a little bit.
# First, we are not interested ever in combining a sample with itself.
# Second, since the matrix is symmetrix, let's just use the upper quadrant.

D <- matrix( nrow = length(sample.names), ncol = length(sample.names), 
             dimnames = list( 
               samples = sample.names,
               samples = sample.names),
             data = Inf)

for (i in 1:(ncol(X)-1)) {  # for every sample (except the last one)
  for (j in (i+1):ncol(X)) {  # for every sample labelled higher than i (including the last one)
    D[i,j] <-  Euclidean.distance( X[,i], X[,j])
  }
}
print(D)

# Now we are ready for the algorithm.


tree <- list()  # we are going to store the tree (denodogram) in a list data structure.
                # at the beginning each sample is a disconnected leaf in the tree represented
                # as a list
for (i in 1:ncol(D)) {
   tree[[i]] <-   c( node_name = colnames(D)[i], distance_to_x = 0, distance_to_y = 0 )
}

D_new <- D      # make a copy of the distance matrix.


while (length(D_new) > 1) {  # we will stop when there is only one sample (column) left. 
                           # That single guy will be the root.
  
  # Step 1: find the minimum entry in the distance matrix
  mn <- which(D_new == min(D_new), arr.ind = TRUE)
  
  # if there are more than one minimum in the matrix, just take the first one.
  # we can these samples x and y
  x <- mn[1,1]; y <- mn[1,2]
  names(x) <- colnames(D_new)[x]; names(y)<- colnames(D_new)[y]

  # Step 2: make the distance from x to the parent and y to parent both equal to 1/2 of the total 
  # between the two samples x and y. 
  
  tree[[length(tree)+1]] <- c( node_name = paste(names(x), names(y), sep=","), 
                              distance_to_x <- D_new[x,y]/2,
                              distance_to_y <- D_new[x,y]/2 )
  
  # Step 3: remove samples x and y and replace with xy; calculate the average distance from xy to all
  # other samples.
 
  D_temp <- D_new   # we will use this matrix to remove samples x and y and add xy
                    # actually we will resuse column/row for sample x and relabel it xy 
                    # we will actually delete column/row for sample y
  
  for (i in 1:(ncol(D_new)-1)) {
    for (j in (i+1):ncol(D_new)) {

          if (i == x) {  # here we need to replace x with xy
            print(i, j)
            D_temp[i, j] <- (D_new[x, j] + D_new[y, j])/2
          }
      
          if (j==x) {
            print(i, j)
            D_temp[i, j] <- (D_new[i, x] + D_new[i, y])/2
            
          }
        
    } # end j
  } # end i
  colnames(D_temp)[x] <- rownames(D_temp)[x] <- paste(names(x), names(y), sep=",")
  D_new <- D_temp[-y, -y]
  
  
} # end of while

# the result is in the tree list