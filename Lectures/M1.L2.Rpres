Module 1, Lecture 2: IT Skills, UNIX
========================================================
author: M Hallett
date: January 2018
autosize:true
font-family: 'Helvetica' 
#output: beamer-presentation 

## Basics of Unix



Some Basic Unix  (not for PCs!)
========================================================

* Log into your account on your Unix/Linux machine or on a Mac laptop/desktop.
* Open (double click) on Applications/Utilities/Terminal

![terminal window](MyFigs/terminal.window.png)


<code><font color="blue">cd</font color></code><br>
<font color="red">_cd = "change directory". When you type this alone, you are sent back to your home._</font color>

<code><font color="blue">cd \~</font color></code><br>
<font color="red">_The tilde is a symbol meaning your home. Executing "cd" and "cd \~" are the same._</font color>

Some Basic Unix  (not for PCs!)
========================================================

<code><font color="blue">ls</font color></code><br>
<font color="red">_"ls = list. This lists all the files & directories in your currently location._</font color>

![terminal window2](MyFigs/terminal.window2.png)

Some Basic Unix
========================================================

<code><font color="blue">cd repos</font color></code><br>
<font color="red">_This moves you down one level of the tree into a directory called repos that's on my machine._</font color><br>
<code><font color="blue">ls</font color></code><br>

![terminal window3](MyFigs/terminal.window3.png)

Some Basic Unix
========================================================
<code><font color="blue">ls -l</font color></code><br>

![terminal window4](MyFigs/terminal.window4.png)

<font color="red">_-l is called a flag. Flags are way to communicate additional information to Unix commands. Here the -l flag for  the ls command tells us which are files and which are directories, permissions (more later), who owns it, size, when it was last modified etc._</font color>

Some Basic Unix
========================================================
<code><font color="blue">cd T4LS; ls -l</font color></code><br>
![terminal window5](MyFigs/terminal.window5.png)
<font color="red">_First I went "down" one more level into the directory called repos. The semicolon ; separtes Unix commands oso you can put multiple commands on the same line._</font color>


Some Basic Unix
========================================================
<code><font color="blue">clear; pwd</font color></code>
<font color="red">_(clear. It clears the screen._</font color>
<font color="red">_print working directory. This tells you where you are in your tree.)_</font color><br>
<code><font color="blue">cd ..</font color></code>
<font color="red">_ (This .. means go up the tree one level.)_</font color> <br>
<code><font color="blue">pwd; cd ../..</font color></code>
<font color="red">_(The / is the symbol for a directory. So this commands says "go up two levels". _</font color><br>
<code><font color="blue">pwd; ls</font color></code><br>
<code><font color="blue">ls michaelhallett</font color></code><br>
<font color="red">_So here I list the contents of the directory but don't change my working directory._</font color><br>
<code><font color="blue">pwd</font color></code> <font color="red">_Btw michaelhallett is my username on the machine._</font color><br>


![terminal window6](MyFigs/terminal.window6.png)


Some Basic Unix
========================================================

<code><font color="blue">cd ~; pwd</font color></code>
<font color="red">_(Back home!)_</font color><br>
<code><font color="blue">cd repos/T4LS/Lectures; ls</font color></code><br>

![terminal window.7](MyFigs/terminal.window.7.png)<br>
<font color="red">_These are the lecture notes for the course. Notice the different file extensions._</font color><br>


Some Basic Unix
========================================================
cat onco.semic*

![terminal window9](MyFigs/terminal.window9.png)<br>
<font color="red">_cat = concatenate. It prints the file to the screen (which is usually referred to as "standard output or std_out)._</font color><br>
<font color="red">_The * is a wildcard character. It tells Unix to find *all* files that start with "onco.semic". In this case there is only one. Consider what would happen if I instead used "cat M1.L*.html"_</font color><br>

Some Basic Unix
========================================================

<code><font color="blue">touch my.new.lecture</font color></code><br>
<code><font color="blue">ls -l</font color></code><br>
<code><font color="blue">cat my.new.lecture</font color></code><br>
![terminal window10](MyFigs/terminal.window10.png)<br>

<font color="red">_touch creates an empty file with the given name._</font color>

Some Basic Unix
========================================================

<code><font color="blue">mv my.new.lecture \~/my.new.lecture.moved</font color></code><br>
<font color="red">_mv = move. This moves the file "my.new.lecture" to a new location (my home directory \~) and a new name "my.new.lecture.moved"._</font color><br>
<code><font color="blue">ls -l ~</font color></code><br>
![terminal window11](MyFigs/terminal.window11.png)<br>


Some Basic Unix
========================================================

<code><font color="blue">cp ~/my.new.lecture.moved new.copy.old.location</font color></code><br>
<font color="red">_cp=copy. It makes a copy (but doesn't destroy the original) of a file. The second argument is the name of the file. You should verify that the original file in ~ is still there. _</font color>


Some Basic Unix
========================================================

<code><font color="blue">rm ~/my.new.lecture.moved</font color></code><br>
<font color="red">_rm=remove. This removes the specified file. Unix might prompt you to be sure._</font color><br>
<code><font color="blue">ls ~</font color></code><br>
![terminal window12](MyFigs/terminal.window12.png)<br>


Some Basic Unix
========================================================

<code><font color="blue">cd ~/repos/T4LS; cp -r ~/repos/T4LS/ course.backup</font color></code><br>

<font color="red">_The -r flag means "recursive": Copy c all the files/directories within T4LS and all the files/directories within each directory within T4LS, etc. etc. etc. to a new location called "course.backup"_</font color>

<code><font color="blue">ls -l</font color></code><br>
![terminal window13](MyFigs/terminal.window13.png)<br>


Some Basic Unix
========================================================

<code><font color="blue">rm -r course.backup</font color></code><br>

<font color="red">_The -r flag again means "recursive". but this will take a long time..._</font color>

<code><font color="blue">rm -r -f course.backup</font color></code><br>
<font color="red">_The -f flag again means force the remove (don't ask for permission)._</font color>

Some Basic Unix
========================================================

<code><font color="blue">mkdir my.new.directory</font color></code><br>
<code><font color="blue">ls</font color></code><br>
<code><font color="blue">cd my.new.directory</font color></code><br>
<code><font color="blue">touch my.new.file</font color></code><br>
<code><font color="blue">ls</font color></code><br>
<code><font color="blue">cd ..</font color></code><br>
<code><font color="blue">rmdir my.new.directory</font color></code><br>
<code><font color="blue">rm -r my.new.directory</font color></code><br>

![terminal window15](MyFigs/terminal.window15.png)<br>

<font color="red">_mkdir = make directory. touch = make new file._</font color>


Some Basic Unix (11)
========================================================
<code><font color="blue">top</font color></code><br>

![terminal window14](MyFigs/terminal.window14.png)<br>

             
<font color="red">_All the processes running on the machine. _</font color>

Action Items
========================================================

Using a Mac or Linux machine, try out the above examples.


BIOL 480 Bioinforamtics (c) M Hallett, CB-Concordia
========================================================
![CB Concordia](MyFigs/sysbiologo.png)



