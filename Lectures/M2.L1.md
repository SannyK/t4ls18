Module 2, Lecture 1: Biological Data Science
========================================================
author: M Hallett
date: February 2018
autosize:true
font-family: 'Helvetica' 
#output: beamer-presentation 

## RNA-seq gene expression, differential expresion, descriptive statistics, hypothesis testing, breast cancer data, visualization, ggplot2


Exploring gene expression data
========================================================
* This is a small subset (~10%) of the TCGA RNA-seq data for breast cancer.
* [https://cancergenome.nih.gov/]([https://cancergenome.nih.gov/](https://cancergenome.nih.gov/) The Cancer Genome Atlass<br>



```r
load("~/repos/T4LS18/data/tcga.rnaseq.small.Rda")
names( tcga.rnaseq.small )
```

```
[1] "exprs"       "transcripts" "clinical"   
```

```r
tcga <- tcga.rnaseq.small # for convenience; shorter name
```

* Exercise: draw on paper what the tcga datastructure looks like.


```r
attach(tcga)
er.pos.pats <- subset(clinical, er) 
er.neg.pats <- subset(clinical, !er)
length(er.pos.pats[,1]); length(er.neg.pats[,1])
```

```
[1] 77
```

```
[1] 16
```

Exploring clinical variables
========================================================

```r
er.pos.pats$id
```

```
 [1] "tcga.65"  "tcga.249" "tcga.175" "tcga.193" "tcga.178" "tcga.158"
 [7] "tcga.197" "tcga.227" "tcga.349" "tcga.534" "tcga.14"  "tcga.535"
[13] "tcga.96"  "tcga.536" "tcga.538" "tcga.539" "tcga.540" "tcga.541"
[19] "tcga.40"  "tcga.364" "tcga.542" "tcga.437" "tcga.544" "tcga.292"
[25] "tcga.545" "tcga.546" "tcga.547" "tcga.549" "tcga.550" "tcga.551"
[31] "tcga.553" "tcga.117" "tcga.290" "tcga.554" "tcga.555" "tcga.517"
[37] "tcga.556" "tcga.285" "tcga.42"  "tcga.557" "tcga.561" "tcga.98" 
[43] "tcga.253" "tcga.562" "tcga.79"  "tcga.12"  "tcga.373" "tcga.564"
[49] "tcga.216" "tcga.565" "tcga.566" "tcga.119" "tcga.108" "tcga.279"
[55] "tcga.360" "tcga.567" "tcga.438" "tcga.568" "tcga.247" "tcga.380"
[61] "tcga.442" "tcga.386" "tcga.570" "tcga.571" "tcga.572" "tcga.573"
[67] "tcga.574" "tcga.577" "tcga.487" "tcga.397" "tcga.578" "tcga.41" 
[73] "tcga.49"  "tcga.213" "tcga.124" "tcga.441" "tcga.580"
```

```r
er.neg.pats$id
```

```
 [1] "tcga.254" "tcga.129" "tcga.377" "tcga.537" "tcga.383" "tcga.543"
 [7] "tcga.552" "tcga.166" "tcga.419" "tcga.558" "tcga.60"  "tcga.563"
[13] "tcga.575" "tcga.576" "tcga.19"  "tcga.372"
```

Exploring exressssion of a gene
========================================================

```r
esr1 <- which(transcripts == "ESR1")
transcripts[esr1,]
```

```
      gene.name   probe.id
19697      ESR1 uc003qom.3
19698      ESR1 uc003qon.3
19699      ESR1 uc003qoo.3
19700      ESR1 uc010kin.2
19701      ESR1 uc010kio.2
19702      ESR1 uc010kip.2
19703      ESR1 uc010kiq.2
19704      ESR1 uc010kir.2
19705      ESR1 uc010kis.2
19706      ESR1 uc010kit.1
19707      ESR1 uc011eet.1
19708      ESR1 uc011eeu.1
19709      ESR1 uc011eev.1
19710      ESR1 uc011eew.1
19711      ESR1 uc011eex.1
19712      ESR1 uc011eey.1
```

```r
exprs[esr1,1:5]
```

```
          tcga.1    tcga.65  tcga.249   tcga.254  tcga.175
 [1,]   345.4259    37.1002  101.0759     0.0000  247.6399
 [2,]   565.9546   115.9628  633.5632   454.5830  608.1732
 [3,] 11898.5692 11832.1396 5974.6516  2911.1977 4320.4205
 [4,]  5239.9002     0.0000 1671.1465     0.0106  800.0820
 [5,]  1875.6179   339.7354  951.9328   122.0014  632.3651
 [6,]   330.1023    17.8547    0.0000     0.0000   47.6557
 [7,]    16.0000    91.3176    0.0000     0.0000    6.9156
 [8,]     0.0000  2584.0484    0.0000     0.0000    0.0000
 [9,]    25.1429     6.6779    8.1247     0.0000   11.2244
[10,]    27.8710    23.1700    9.1891     0.0000    8.5326
[11,] 16885.8988  3542.8998 8960.3508 10146.2757 4466.4639
[12,]     0.0000     0.0338    0.0000     0.0000    0.0000
[13,]    61.0444     0.0000 1351.2404  1693.4138 1841.9032
[14,]     8.1106     0.0000    0.0000   676.9642  215.9682
[15,]     9.9027     0.0000    0.0000     0.0000    0.0000
[16,]  4572.1621  2816.1374 2057.8057   791.5028 2131.1059
```

Exploring exressssion of a gene
========================================================

```r
exprs[esr1,er.pos.pats$id]
```

```
         tcga.65  tcga.249  tcga.175  tcga.193  tcga.178   tcga.158
 [1,]    37.1002  101.0759  247.6399    0.0000    0.0000   428.2937
 [2,]   115.9628  633.5632  608.1732    0.0000   96.3135  2364.1029
 [3,] 11832.1396 5974.6516 4320.4205   29.3792 1396.8895  5580.7920
 [4,]     0.0000 1671.1465  800.0820    0.0000  489.5064  2030.0297
 [5,]   339.7354  951.9328  632.3651    0.0000  127.7447  1096.1966
 [6,]    17.8547    0.0000   47.6557    0.0000    0.0000   108.7303
 [7,]    91.3176    0.0000    6.9156    0.0000    0.0000    24.0994
 [8,]  2584.0484    0.0000    0.0000   21.1604    0.0000     0.0000
 [9,]     6.6779    8.1247   11.2244    0.0000   23.4342    36.8057
[10,]    23.1700    9.1891    8.5326    2.9663    0.0000    25.0697
[11,]  3542.8998 8960.3508 4466.4639 2276.9467 7979.1056 18634.8206
[12,]     0.0338    0.0000    0.0000   11.7319    0.0000     0.0000
[13,]     0.0000 1351.2404 1841.9032    0.0000    0.3923  3534.9051
[14,]     0.0000    0.0000  215.9682    0.0000  253.5131     0.0103
[15,]     0.0000    0.0000    0.0000    0.0000    0.0000     0.0000
[16,]  2816.1374 2057.8057 2131.1059    9.3554  500.0949  3587.4583
        tcga.197   tcga.227  tcga.349   tcga.534   tcga.14   tcga.535
 [1,]    39.6953     0.0000    0.0000    16.4950    0.0000     0.0000
 [2,]   637.8207    70.8330   11.8833   130.3583    0.0000    31.5381
 [3,]  3639.8916   359.8096   67.7386  2129.1689  220.4605   759.4297
 [4,]  3754.6393    25.8585    0.0000   282.2412   83.2434   104.7272
 [5,]   773.5269    91.6358    0.0000   189.3927    0.0000    93.1745
 [6,]     0.0000     0.0000    0.0000     0.0000    0.0000     0.0000
 [7,]    19.6561     0.0000    0.0000     0.0000    0.0000     0.0000
 [8,]   790.4680     3.8648    0.0000   171.1306    0.1184    84.9745
 [9,]     0.0000     0.0000    0.0000     0.0000    0.0000     0.0000
[10,]    21.2219    24.7119    4.7569     0.0000    7.8684    17.9975
[11,] 14720.5599 12162.0305 5759.0051 22862.9426 1012.6776 19376.7685
[12,]     0.0000     0.0000    0.0000   173.7091  153.3750    54.6473
[13,]   720.8121     0.0000    0.0000   662.6750  640.6250     0.0000
[14,]     1.4652     0.0000    0.0000    12.3149  434.9539     0.0000
[15,]     0.0000     0.0000    0.0000     0.0000  117.7237     0.0000
[16,]  1540.7428     1.0271    0.0000  1070.3341  454.6118    13.5524
         tcga.96  tcga.536   tcga.538  tcga.539   tcga.540  tcga.541
 [1,]   723.5844    0.0000   127.6963    0.0000   425.1181    0.0000
 [2,]  6021.5758  130.8057  3341.9870  334.1303   899.1148  140.6901
 [3,]  7640.3530 1431.0656 16464.3113 4457.2588  5222.0838  399.6470
 [4,]  7149.7283    0.0000   586.8435    0.0000  5859.0217  538.8052
 [5,]  1826.9609   58.1982  2035.7739  286.1144   995.0120   85.2714
 [6,]   577.4521    0.0000   416.6333    0.0000   153.6365    0.0000
 [7,]    11.5840    0.0000    92.3007   16.9880    24.5164    0.0000
 [8,]     0.0000  350.8593   824.2066    0.0000     0.0000    0.0000
 [9,]    17.8905    0.0000    10.7717    2.7339    17.2370   34.4862
[10,]    33.8483    1.4251    24.5409    0.0000    19.6880    0.0000
[11,] 11607.0080 4842.1480 11830.3365 2586.8813 15116.4455 9555.3769
[12,]     0.0000    0.0000     0.0000    0.0000     0.0000    0.0000
[13,]     4.0726  933.9742     0.0038   16.6374   514.7192  784.6402
[14,]     4.5108    0.5566    10.7642  765.0131     2.7591  232.4484
[15,]     0.0000    0.0000    29.3593    0.0000     0.0000    0.0000
[16,]  5182.1317  603.3204  4441.8780 1151.0490  2609.4642  344.9753
         tcga.40  tcga.364  tcga.542  tcga.437  tcga.544   tcga.292
 [1,]     9.4561   26.6128   23.3242    0.0000    0.0000    17.0367
 [2,]   773.9924  205.6017  291.4643   81.8704   59.6948   213.2755
 [3,]  5623.6925 2768.3974 1704.2080 1150.5131  723.8056  7962.1350
 [4,]  1822.1001  956.9310  314.9201    0.0000    0.0000  2735.5463
 [5,]   415.3101  373.1536  108.0685  223.8535   12.1676   777.2061
 [6,]   157.6877    0.0000    0.0000    0.0000    0.0000   213.6988
 [7,]     0.0000   13.3950   67.9547    0.0000    0.0000     0.0000
 [8,]     0.0000    0.0000  406.6650    0.0000  269.8803     0.0000
 [9,]     3.1368    2.6588    0.0000    0.0000    0.0000    28.5057
[10,]     8.8231    4.8659    3.0325    0.0000    3.1151    17.7185
[11,] 10620.5621 3181.5336 2694.3766 3484.1145 5219.7137 23148.2004
[12,]     0.0000    0.0000    0.0000    0.0000    0.0000     0.0000
[13,]   871.8980    0.0000  367.1487  312.4502  223.3840  2894.2275
[14,]     0.0457    0.0675   44.2322  369.2590    0.0000     0.0000
[15,]     0.0000    0.0000    0.0000    0.0000    0.0000     0.0000
[16,]  2214.9954  929.6387  513.7977  476.5092  255.8400  3202.9891
       tcga.545 tcga.546   tcga.547   tcga.549   tcga.550  tcga.551
 [1,]   58.5627  29.7083   455.0943    13.8423   119.9575    0.0000
 [2,]  249.8115   0.0000   200.9589   428.2522   524.1921   74.8616
 [3,] 2290.1744 152.1316  8671.5513     0.0000  8767.2140 1731.2435
 [4,]  610.6823  21.5571   491.1552  2221.3350   257.6187  328.7629
 [5,]  452.2979  19.5392  1431.1003   576.4539   800.0044  146.9869
 [6,]    0.0000   0.0000   124.1963     0.0000     0.0000    0.0000
 [7,]    0.0000   0.0000    24.1336     0.0000    45.4527   31.7040
 [8,]    0.0000   0.0000     0.0000   488.8714     0.0000    0.0000
 [9,]    1.9792   0.0000    38.7133   136.1134    14.9179    3.3247
[10,]   15.2524   0.0000    13.7851     3.4409    14.8820    9.7050
[11,] 5431.2297 148.3518 15134.1792 25430.5224 14150.0917 5561.1703
[12,]    0.0000   0.0000     0.0000     0.0000     0.0000    0.0000
[13,]  560.1751  22.5430   756.5680    43.4740   938.1064    0.0000
[14,]    0.0051   1.9001    91.8686    14.3074   631.1487  116.4429
[15,]    0.0000   0.0000    12.9974     0.0000     0.0000    0.0000
[16,]  632.2784  84.1696  4662.6897  1786.9893  2871.1531  378.0528
       tcga.553  tcga.117 tcga.290  tcga.554  tcga.555  tcga.517  tcga.556
 [1,]   29.5570   11.5197  37.7187   62.0035  162.0754    0.0000   35.0340
 [2,]  298.0528   14.5746   9.6214  675.1142  621.1154  165.4469  530.5967
 [3,] 9326.2526   69.6021   0.5669 9191.5114 3003.6171  701.7084 7245.5493
 [4,]  715.3460   79.3605   0.0000  480.1347 3031.6603  181.2235  592.9521
 [5,]  668.9104    0.0000   0.0000  910.4517  747.8660   60.8021  519.0446
 [6,]  115.8947    0.0000   0.0000    0.0000   93.2417    0.0000    0.0000
 [7,]   68.7006    0.0000   0.0000   43.0960   27.0150    0.0000   67.4983
 [8,] 1471.7107    0.0000  27.4162    0.0000    0.0000   63.9017   34.1787
 [9,]    9.9563    2.1349   0.0000    6.2233    0.0000    3.5006   13.7774
[10,]   17.7257    0.9515   0.3141   10.8188    7.1284    3.0452   14.1698
[11,] 7805.3689 1080.6428 134.6870 8704.4134 7615.4819 7713.5516 6188.7731
[12,]    0.0000    0.0000   0.0000    0.0000    0.0000    0.0000    0.0000
[13,]  366.8882   86.2841   0.0000  629.1441    0.2224    0.0000 1435.7405
[14,]   39.8751    0.0000   0.0000    9.1325    0.3427  200.2696   49.8777
[15,]    0.0000    0.0000   0.0000    0.0000    0.0000    0.0000    0.0000
[16,] 2996.2250   74.2572   8.2918 3349.5364 1492.8443  260.3580 2537.0405
        tcga.285    tcga.42  tcga.557   tcga.561   tcga.98  tcga.253
 [1,]    10.1690   113.7393   27.3094    74.4888  285.7864   91.1566
 [2,]   285.0317  1071.5357  271.3928    39.1194   13.2311   70.2718
 [3,]  4610.4660 12398.6194 2899.5222   743.8558  803.4666 3275.0441
 [4,]    20.8981  7117.8974   84.2487     0.0000    0.0000    0.0000
 [5,]   354.7276  2058.6695  153.8452     8.9662   34.5914  271.7601
 [6,]     0.0000   189.1724    0.0000     0.0000    0.0000   55.1636
 [7,]    20.6231    75.9618   37.8434     0.0000    0.0000   18.9214
 [8,]     0.0000     0.0000  958.9152     0.0051    0.0000    0.0000
 [9,]     6.9508     2.7905    0.0000     0.0000    0.0000    1.4387
[10,]     9.6954    38.4564    2.8863    21.5292    3.5767   14.5441
[11,] 12029.7227 10515.4484 4740.4455 11196.7575 1068.4497 4609.9468
[12,]     0.0000     0.0000    0.0000     0.0000    0.0000  543.1747
[13,]   702.5198     0.6257    0.0000     0.0000    0.0000 2231.9029
[14,]     0.0000    35.2029    0.0000     0.0000    0.0000  323.8233
[15,]     0.0000     0.0000    0.0000     0.0000    0.0000    0.0000
[16,]  1590.5456  4576.4287  847.7407   134.9177  376.0337 1128.2195
        tcga.562    tcga.79   tcga.12  tcga.373  tcga.564   tcga.216
 [1,]   106.9409    54.6957    8.0355   22.5677  107.7206    28.3007
 [2,]   735.8855   129.2401   95.9017  345.1687  242.2721   855.1146
 [3,]  4295.4085  7535.5350 1304.6690 4707.8062 1353.1591 16721.7359
 [4,]  2073.7655     0.0000   30.3265    0.0000  566.1002   879.5482
 [5,]   688.5793   861.2666    0.0000  517.2338  254.4645  2185.5598
 [6,]     0.0000    86.5565    0.0000    0.0000    0.0000   107.2674
 [7,]     0.0000    12.2839    0.0000    0.0000    7.1065    20.4618
 [8,]   829.2556     0.0000  103.4333    0.0000    0.0000  2265.4136
 [9,]     0.0000     8.2402    0.0000    7.8799    7.2817    11.7658
[10,]     9.9209     2.2300   10.3689   16.0685    0.0000    23.9252
[11,] 19371.0791 13904.1286 1559.5968 9147.5117 3044.8943  7338.5133
[12,]    60.3735     0.0000    0.0000    0.0000    0.0000     0.0000
[13,]     0.0000     0.0000  718.9452    5.2727 1103.3364   709.1113
[14,]     0.0000     0.5182  232.8923    0.0042   15.5518     0.0000
[15,]     0.0000     0.0000   53.6945    0.0000    0.0000     0.0000
[16,]  1739.1806  3131.4149 1220.5306 1049.1302  593.2438  3111.0847
       tcga.565  tcga.566  tcga.119  tcga.108   tcga.279   tcga.360
 [1,]    0.0000    0.0000    0.0000    0.0000    34.5536    28.2140
 [2,]   11.8032  376.4736  133.3910    0.0000  3951.3195  1197.8191
 [3,]   92.3056 7810.8439 4953.4386   78.1850  3257.0626  9117.7385
 [4,]   12.0670    0.0000  679.5517    0.0000  2155.3957  1245.8319
 [5,]    0.0000  365.6486  665.1083   17.4066   732.6110  1384.0242
 [6,]    0.0000   29.5221    0.0000    0.0000    98.5417   264.4749
 [7,]    0.0000    0.0000    0.0000    0.0000    33.9809    69.3937
 [8,]    0.0000    0.0000    0.0000    0.0000   864.0800     0.0000
 [9,]    0.0000    0.0000    1.7119    1.4299    18.9914    20.8620
[10,]   11.9930    8.4643    5.6075    7.5676    21.7065    11.7428
[11,] 3370.6664 9220.8632 4614.4260 4390.9250 22526.8237 21876.6241
[12,]    0.0000    0.0000  231.3205    9.8957     0.0000     0.0000
[13,]    0.0000    0.0042    0.0000  199.5561  3458.3706   846.4524
[14,]    0.0000    0.2100    0.0000  141.9833     0.0106     0.1415
[15,]    0.0000    0.0000    0.0000    0.0000    12.7835     0.0000
[16,]    0.0000 1450.8368  482.4829   24.5148  3543.6445  3866.6485
        tcga.567 tcga.438  tcga.568  tcga.247  tcga.380  tcga.442
 [1,]    28.0723  45.1239   18.8439   10.7321    0.0000    0.0000
 [2,]   352.3612  31.5444  247.2624    7.0010    0.0000  591.9682
 [3,]  3026.7875 460.4758 1278.1097   43.9766  484.1490 4797.1150
 [4,]   337.4119  15.5228  306.2467    0.0000   51.3285   18.5718
 [5,]   463.9819  45.3827   25.4424   28.7877   41.4795  629.6305
 [6,]     0.0000   0.0000    0.0000    0.0000    0.0000    0.0000
 [7,]     0.0000   0.0000    0.0000    0.0000    0.0000   21.0687
 [8,]     0.0000   0.0000  296.3333   21.2089    0.0000    0.0000
 [9,]    14.4204   4.0136    0.0000    0.0000    0.0000   19.1689
[10,]     4.9947   3.1290    0.0000    1.6080    0.0000   17.4932
[11,] 12781.1354 620.1505  867.5783 3337.2063 1292.8580 7903.6937
[12,]     0.0000   0.0000    0.0000   17.4240    0.0000   39.9988
[13,]  1226.6852 211.4237  259.4497  311.7556   13.8622  552.3314
[14,]     0.0000  51.9560    0.0000    0.0000    0.0000    0.1605
[15,]     0.0000   0.0000    0.0000    0.0000    0.0000    0.0000
[16,]  1440.7305 132.7274  592.8140   10.1364  241.2222 1803.9189
       tcga.386  tcga.570   tcga.571  tcga.572  tcga.573  tcga.574
 [1,]  112.5294   45.1007   210.5474   22.6414    0.0000    0.0000
 [2,] 1277.3919  150.5864   316.3547  537.9781   23.8656   78.4890
 [3,] 4539.5366 4211.5698  5866.7884 2660.6485   50.0407  662.4689
 [4,] 6173.7759    0.0000     0.0000    1.1681    0.0000  259.2444
 [5,] 1331.8347  151.5836   413.6766  466.5953    0.0000   85.1373
 [6,]  130.4206    0.0000     0.0000    0.0000    0.0000    0.0000
 [7,]   67.1841   20.7826    22.6518    0.0000    0.0000    0.0000
 [8,]    0.0000  375.9176   583.8642    0.0000   47.8473    0.0000
 [9,]    2.3493    0.0000     0.0000    5.8090    1.4114    0.0000
[10,]    7.6282    6.1736    14.4487   11.4667    5.1507    3.7008
[11,] 7710.1110 5091.8898 11429.0195 7515.0371 4730.3340 1592.0084
[12,]    0.0000   41.9096     0.0000    0.0000    0.0000    0.0000
[13,]  516.6659  311.6933   974.9560 1497.6875    0.0000  738.3392
[14,]   73.3751    0.0045   506.4470    0.0000    0.0000  125.5238
[15,]    0.0000    0.0000     0.0000    0.0000    0.0000    0.0000
[16,] 2713.3309 1047.9087  1586.0761 1456.5460    0.0000  277.8981
        tcga.577   tcga.487  tcga.397  tcga.578   tcga.41    tcga.49
 [1,]     9.0200    96.0541   94.7719   32.8009    0.0000   177.8916
 [2,]   141.9835   380.9986  709.6618   98.5503  117.8679   270.2700
 [3,]   867.6560  2648.2920 3406.6514 1782.5974 2246.3382  6464.0978
 [4,]     0.0000   414.5227  770.8274    0.0000  525.0714  1585.5371
 [5,]    81.6016   379.4556  691.8888  142.5803  192.4967  1051.6749
 [6,]     0.0000     0.0000    0.0000    0.0000    0.0000     0.0000
 [7,]    38.1982     0.0000    0.0000    0.0000   16.8638    13.8090
 [8,]   384.8277     0.0000  594.4531    0.0000    0.0000     0.0000
 [9,]     0.0000     2.1678    1.8958    0.0000    7.9768    26.5632
[10,]    72.7578    15.6888    5.0686    0.0000    0.0000    12.4621
[11,] 13856.3447 10291.5187 7630.5330 5659.0471 1824.1732 24143.5346
[12,]     0.0000     0.0000    0.0000    0.0000    0.0000     0.0000
[13,]     0.0000  1106.8873   53.7228  458.3833  480.1371  1840.2502
[14,]     0.0000     0.0000    0.0000    0.0964    0.1356     0.0131
[15,]     0.0000     0.0000    0.0000    0.0000    0.0000     0.0000
[16,]   100.3507  1049.0287  877.7245  558.0578  569.9557  2796.3036
        tcga.213  tcga.124  tcga.441  tcga.580
 [1,]    87.1255   32.4129   91.7660   20.4894
 [2,]   655.9053  555.8255  921.2956 1163.1115
 [3,] 14642.7437 4691.5741 2472.0232 4855.0086
 [4,]  1690.6104 4793.9785  331.6840 2644.4399
 [5,]  2011.1525  940.8844  330.0372  396.9010
 [6,]     0.0000    0.0000    0.0000    0.0000
 [7,]   107.7792    0.0000    0.0000   23.5557
 [8,]     0.0000    0.0000    0.0000 1537.1322
 [9,]    17.3069   12.6369    1.6393    3.6927
[10,]     8.7543    2.8720    2.6996   17.9187
[11,] 15720.0713 8749.9108 5115.5995 6325.8200
[12,]     0.0000    0.0000    0.0000    0.0000
[13,]  2886.9826 1629.8899  753.4534    0.0000
[14,]   713.1111    0.0039    0.1429    0.0545
[15,]     0.0000    0.0000    0.0000    0.0000
[16,]  4201.2429 3239.4412 1035.7860 2260.0003
```

```r
exprs[esr1,er.neg.pats$id]
```

```
        tcga.254 tcga.129 tcga.377 tcga.537 tcga.383 tcga.543 tcga.552
 [1,]     0.0000   4.1893   0.0000   4.2623   0.0000   0.0000   4.5178
 [2,]   454.5830   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [3,]  2911.1977   0.0000   0.0000   0.0000   0.0000  48.1403   0.0000
 [4,]     0.0106   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [5,]   122.0014   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [6,]     0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [7,]     0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [8,]     0.0000   0.0000   5.2843   0.0000  13.2888  19.5099   0.0000
 [9,]     0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
[10,]     0.0000   0.2854   1.2246   0.1177   0.0000   7.7920   0.0000
[11,] 10146.2757  59.2307  24.4377  22.8227   0.0000  28.7946   3.9027
[12,]     0.0000   0.0000   0.0000   0.0000  14.5530  42.8561   0.0000
[13,]  1693.4138   0.0000  22.9314   0.0000   0.0000  57.4201   0.0000
[14,]   676.9642   0.0000   0.0000  15.7738   0.0000   0.0000   0.0000
[15,]     0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
[16,]   791.5028   0.0000   0.0000   3.7246   0.0000   0.0000   0.0000
      tcga.166 tcga.419 tcga.558  tcga.60 tcga.563 tcga.575 tcga.576
 [1,]   0.0000   0.0000   0.0000   0.0000   2.5754   7.8168   6.3831
 [2,]   0.0000  13.5355   5.6580   0.0000   0.0000   0.0000   0.0000
 [3,]   0.0000  69.9873   0.0000 132.4640   0.0000   0.0000   0.0000
 [4,]   0.0000   0.0000   0.5464  27.1174   0.0000   0.6029   0.0000
 [5,]   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [6,]   0.0000   0.0000   5.9346   0.0000   0.0000  19.8711   0.0000
 [7,]   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
 [8,]   0.0000   7.7400   6.6920   0.0000   3.4479   0.0000   2.8845
 [9,]   0.0000   0.0000   0.0000   0.0000   0.0000   0.3915   0.0000
[10,]   0.0000   0.0000   0.6813   2.8666   0.0000   0.0000   0.4121
[11,]  19.3856  73.2740  20.6052 161.1808   3.2517  48.7267   0.0000
[12,]   0.0000   0.0000   0.0000   0.0000   7.5930   0.0000   0.0000
[13,]   0.0000  45.3919   0.0000  22.7128   0.0000  36.3917  29.2533
[14,]   0.0000   0.0000   0.0000  52.6607  31.4829  34.7909   0.0000
[15,]   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000   0.0000
[16,]   0.0000  82.2139   0.0000 130.6372   0.0000  23.2702   3.9230
      tcga.19 tcga.372
 [1,]  0.0000   0.0000
 [2,]  9.1150   0.0000
 [3,] 26.5343   0.0000
 [4,]  0.0000   0.0300
 [5,]  0.0000   0.0000
 [6,]  0.0000   7.6789
 [7,]  0.0000   0.0000
 [8,]  2.9924   0.0000
 [9,]  0.0000   0.8928
[10,]  0.0000   1.3993
[11,] 39.5027 121.0765
[12,] 10.0387  30.6941
[13,]  0.0000  47.2580
[14,]  0.0000   0.0000
[15,]  0.0000   0.0000
[16,]  0.0000   0.4292
```

Exploring exressssion of a gene
========================================================
* For simplicity, let's work at the gene level and forget about different isoforms/transcripts.
* simply sum the expression values over all transcripts for each patient.


```r
esr1.er.pos <- apply( exprs[esr1,er.pos.pats$id], MARGIN=2, sum )
(esr1.er.neg <- apply( exprs[esr1,er.neg.pats$id], MARGIN=2, sum ))
```

```
  tcga.254   tcga.129   tcga.377   tcga.537   tcga.383   tcga.543 
16795.9492    63.7054    53.8780    46.7011    27.8418   204.5130 
  tcga.552   tcga.166   tcga.419   tcga.558    tcga.60   tcga.563 
    8.4205    19.3856   292.1426    40.1175   529.6395    48.3509 
  tcga.575   tcga.576    tcga.19   tcga.372 
  171.8618    42.8560    88.1831   209.4588 
```

```r
exprs[esr1, er.neg.pats$id[1]] # just for patient tcga.254
```

```
 [1]     0.0000   454.5830  2911.1977     0.0106   122.0014     0.0000
 [7]     0.0000     0.0000     0.0000     0.0000 10146.2757     0.0000
[13]  1693.4138   676.9642     0.0000   791.5028
```
* Exercise: do this with a for loop instead of apply()


Differential Expression: Parametric statistical tests, t-test
========================================================
- Alternative Hypothesis $H_1$: $\mu_{good} - \mu_{bad} \neq 0$</code>.
- Null Hypothesis $H_0$:  $\mu_{good} - \mu_{bad} = 0$.

```r
t.test(esr1.er.pos, esr1.er.neg, var.equal=TRUE) # two sample t-test
```

```

	Two Sample t-test

data:  esr1.er.pos and esr1.er.neg
t = 5.1539, df = 91, p-value = 1.474e-06
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
  9712.628 21894.221
sample estimates:
mean of x mean of y 
16968.612  1165.188 
```

Differential Expression: Parametric statistical tests, t-test
========================================================
- Alternative Hypothesis $H_1$: $\mu_{good} - \mu_{bad} \neq 0$</code>.
- Null Hypothesis $H_0$:  $\mu_{good} - \mu_{bad} = 0$.

```r
t.test(esr1.er.pos, esr1.er.neg)   # by default differences in variance are not considered (Welch test)
```

```

	Welch Two Sample t-test

data:  esr1.er.pos and esr1.er.neg
t = 9.1558, df = 70.507, p-value = 1.287e-13
alternative hypothesis: true difference in means is not equal to 0
95 percent confidence interval:
 12361.35 19245.50
sample estimates:
mean of x mean of y 
16968.612  1165.188 
```

Differential Expression: Non-parametric statistical tests, Wilcoxon
========================================================
- Alternative Hypothesis $H_1$: The location shift is not equal to 0.
- Null Hypothesis $H_0$:  It is equal to 0.

```r
wilcox.test(esr1.er.pos, esr1.er.neg)
```

```

	Wilcoxon rank sum test with continuity correction

data:  esr1.er.pos and esr1.er.neg
W = 1185, p-value = 7.166e-09
alternative hypothesis: true location shift is not equal to 0
```

Differential Expression: Non-parametric statistical tests, Kolmogorov-Smirnoff
========================================================
- Alternative Hypothesis $H_1$: (Intuitively) the cdf of tmp.good is not equal to the cdf of tmp.bad.<br>
- Null Hypothesis $H_0$:  They are equal.

```r
ks.test(esr1.er.pos, esr1.er.neg)
```

```

	Two-sample Kolmogorov-Smirnov test

data:  esr1.er.pos and esr1.er.neg
D = 0.91153, p-value = 4.426e-13
alternative hypothesis: two-sided
```
Differential Expression: Non-parametric statistical tests, Kolmogorov-Smirnoff
========================================================
* A note here: one can assign the results of such tests and query the individual pieces of information
related to the test.


```r
ks.result <- ks.test(esr1.er.pos, esr1.er.neg)
names(ks.result)
```

```
[1] "statistic"   "p.value"     "alternative" "method"      "data.name"  
```

```r
ks.result$p.value
```

```
[1] 4.426459e-13
```
 

Visualizaton with ggplot2
========================================================
* There are some basic plotting functions in R but they are limited.
* The ggplot2 package is  powerful.
* [http://ggplot2.org/resources/2007-vanderbilt.pdf]([http://ggplot2.org/resources/2007-vanderbilt.pdf](http://ggplot2.org/resources/2007-vanderbilt.pdf) ggplot2 tutorial from the creator<br>
* 

```r
#install.packages("ggplot2")   # get it from CRAN
library(ggplot2)
```


We explore our RNA-seq data in ggplot2
========================================================
* In particular, we'll look at the the expression of ESR1 and HER2 in relation
to different clinical variables.
* We need to do a bit of work to get our data.frame ready.
* we will add columns to tcga$clinical


```r
clinical$esr1.expr <- apply( exprs[esr1,], MARGIN=2, sum )
clinical$her2.expr <- apply( exprs[which(transcripts == "ERBB2"),], MARGIN=2, sum )
head(clinical)
```

```
               id      id.orig geo.sample    er  her2 size stage grade
tcga.1     tcga.1 TCGA-E9-A1RC       <NA>    NA    NA   NA     4    NA
tcga.65   tcga.65 TCGA-AC-A8OP       <NA>  TRUE    NA   NA     1    NA
tcga.249 tcga.249 TCGA-BH-A0B1       <NA>  TRUE FALSE   NA     2    NA
tcga.254 tcga.254 TCGA-B6-A0RG       <NA> FALSE    NA   NA     3    NA
tcga.175 tcga.175 TCGA-D8-A1JH       <NA>  TRUE FALSE   NA     1    NA
tcga.193 tcga.193 TCGA-A2-A0CR       <NA>  TRUE FALSE   NA     3    NA
         lymph age      time event    time.5 event.5 chemo tamoxifen
tcga.1    TRUE  56 40.241096 FALSE 40.241096   FALSE  TRUE      TRUE
tcga.65  FALSE  72  1.742466 FALSE  1.742466   FALSE    NA        NA
tcga.249  TRUE  66 37.742466 FALSE 37.742466   FALSE  TRUE     FALSE
tcga.254 FALSE  26 68.449315 FALSE 60.000000   FALSE    NA        NA
tcga.175 FALSE  56  9.336986 FALSE  9.336986   FALSE FALSE      TRUE
tcga.193 FALSE  54 97.150685 FALSE 60.000000   FALSE  TRUE      TRUE
         herceptin  intclust    type pam50.authors pam50.inhouse
tcga.1       FALSE intclust8 IDC/ILC          <NA>          <NA>
tcga.65         NA intclust1     IDC          <NA>          <NA>
tcga.249     FALSE intclust9     IDC          <NA>          <NA>
tcga.254        NA intclust7     IDC          <NA>          <NA>
tcga.175     FALSE intclust4     IDC          <NA>          <NA>
tcga.193     FALSE intclust4     ILC          <NA>          <NA>
         pam50.genefu pam50.parker claudin.low lehmann   cit    hybrid
tcga.1           LumA         LumA        <NA>    <NA>  lumA      <NA>
tcga.65          LumA         LumA        <NA>    <NA> normL  erp.lumA
tcga.249         LumA         LumA        <NA>    <NA>  lumA  erp.lumA
tcga.254         LumA         LumA        <NA>    <NA>  lumB      <NA>
tcga.175         LumA         LumA        <NA>    <NA> normL  erp.lumA
tcga.193       Normal       Normal        <NA>    <NA> normL erp.normL
         esr1.expr her2.expr
tcga.1    41861.70  7370.413
tcga.65   21407.08 12583.896
tcga.249  21719.08  6490.310
tcga.254  16795.95 10141.767
tcga.175  15338.45  9154.781
tcga.193   2351.54  4608.364
```


qplot, a simple way to work with ggplot2
========================================================
* qplot wraps up all the details of ggplot with a
familiar syntax borrowed from plot
* Additional features:<br>
• Automatically scales data<br>
• Can produce any type of plot<br>
• Facetting and margins<br>
• Creates objects that can be saved and modified



qplot, a simple way to work with ggplot2
========================================================
.


```r
qplot(clinical$esr1.expr, clinical$her2.expr)
```

![plot of chunk unnamed-chunk-14](M2.L1-figure/unnamed-chunk-14-1.png)
***

.


```r
qplot(esr1.expr, her2.expr, data = clinical)
```

![plot of chunk unnamed-chunk-15](M2.L1-figure/unnamed-chunk-15-1.png)
qplot, a simple way to work with ggplot2
========================================================
.


```r
qplot(esr1.expr, er, data = clinical)
```

![plot of chunk unnamed-chunk-16](M2.L1-figure/unnamed-chunk-16-1.png)
***

.


```r
qplot(her2.expr, her2, data = clinical)
```

![plot of chunk unnamed-chunk-17](M2.L1-figure/unnamed-chunk-17-1.png)

qplot
========================================================
.


```r
qplot(esr1.expr, her2.expr, data = clinical, color = er)
```

![plot of chunk unnamed-chunk-18](M2.L1-figure/unnamed-chunk-18-1.png)
***

.


```r
qplot(esr1.expr, her2.expr, data = clinical, color = her2)
```

![plot of chunk unnamed-chunk-19](M2.L1-figure/unnamed-chunk-19-1.png)

qplot
========================================================
.


```r
qplot(esr1.expr, her2.expr, data = clinical, color = pam50.parker)
```

![plot of chunk unnamed-chunk-20](M2.L1-figure/unnamed-chunk-20-1.png)
***

.


```r
qplot(esr1.expr, her2.expr, data = clinical, color = tamoxifen)
```

![plot of chunk unnamed-chunk-21](M2.L1-figure/unnamed-chunk-21-1.png)

qplot
========================================================
.


```r
qplot(esr1.expr, age, data = clinical)
```

![plot of chunk unnamed-chunk-22](M2.L1-figure/unnamed-chunk-22-1.png)
***

.


```r
qplot(esr1.expr, age, data = clinical,
      geom=c("point", "smooth"), method=lm)
```

![plot of chunk unnamed-chunk-23](M2.L1-figure/unnamed-chunk-23-1.png)
qplot
========================================================
.


```r
qplot(esr1.expr,  data = clinical,
        geom="histogram" )
```

![plot of chunk unnamed-chunk-24](M2.L1-figure/unnamed-chunk-24-1.png)
***

.


```r
qplot(her2.expr,  data = clinical,
        geom="histogram" )
```

![plot of chunk unnamed-chunk-25](M2.L1-figure/unnamed-chunk-25-1.png)

The ggplot function 
========================================================
.
* ggplot is more complicated than qplot but ...
* gives you more control.



```r
d <- ggplot(clinical,
 aes(x=esr1.expr, y=her2.expr))
d + geom_point()
```

![plot of chunk unnamed-chunk-26](M2.L1-figure/unnamed-chunk-26-1.png)
***

.


```r
d + geom_point(aes(colour = age))
```

![plot of chunk unnamed-chunk-27](M2.L1-figure/unnamed-chunk-27-1.png)


The ggplot function: scatterplot
========================================================
.



```r
d + geom_point(aes(colour = event.5)) + scale_colour_brewer()
```

![plot of chunk unnamed-chunk-28](M2.L1-figure/unnamed-chunk-28-1.png)
The ggplot function: histograms
========================================================
.


```r
ggplot(clinical) + geom_histogram(aes(x=age))
```

![plot of chunk unnamed-chunk-29](M2.L1-figure/unnamed-chunk-29-1.png)
***


```r
ggplot(clinical) +  geom_histogram(aes(x=esr1.expr))
```

![plot of chunk unnamed-chunk-30](M2.L1-figure/unnamed-chunk-30-1.png)

The ggplot function: histograms
========================================================
.


```r
p <- ggplot(clinical, aes(x=esr1.expr))
p + geom_histogram()
```

![plot of chunk unnamed-chunk-31](M2.L1-figure/unnamed-chunk-31-1.png)
***


```r
p + stat_bin(geom="area")
```

![plot of chunk unnamed-chunk-32](M2.L1-figure/unnamed-chunk-32-1.png)

The ggplot function: histograms
========================================================
.


```r
p + geom_histogram(aes(fill = pam50.parker))
```

![plot of chunk unnamed-chunk-33](M2.L1-figure/unnamed-chunk-33-1.png)

The ggplot function: stripchart
========================================================
.


```r
stripchart<-ggplot(clinical, aes(pam50.parker, esr1.expr))+geom_point()
stripchart
```

![plot of chunk unnamed-chunk-34](M2.L1-figure/unnamed-chunk-34-1.png)
***

Introduce some "jitter" to better show the density of points

```r
stripchart<-ggplot(clinical, aes(pam50.parker, esr1.expr))+
  geom_jitter(position = position_jitter(width = 0.2),size=5)
stripchart
```

![plot of chunk unnamed-chunk-35](M2.L1-figure/unnamed-chunk-35-1.png)


The ggplot function: stripchart
========================================================
.


```r
stripchart<-ggplot(clinical, aes(pam50.parker, esr1.expr))+geom_point()
stripchart
```

![plot of chunk unnamed-chunk-36](M2.L1-figure/unnamed-chunk-36-1.png)
***

Introduce some "jitter" to better show the density of points

```r
stripchart<-ggplot(clinical, aes(pam50.parker, esr1.expr, col = pam50.parker))+
  geom_jitter(position = position_jitter(width = 0.2),size=5)
stripchart
```

![plot of chunk unnamed-chunk-37](M2.L1-figure/unnamed-chunk-37-1.png)



The ggplot function: stripchart
========================================================
.


```r
stripchart<-stripchart + xlab("Patient Subtype")+ylab("ESR1 expression")
stripchart
```

![plot of chunk unnamed-chunk-38](M2.L1-figure/unnamed-chunk-38-1.png)

heatmaps 
========================================================
.


```r
# install.packages("gplots")  # different than ggplot2
library(gplots)

# first let's prepare our expression data
# step 1: collapse transripts to a single gene.
# If you want to see the code I used, it is in:
# ~/repos/T4LS18/src/M2.L1.transcript2gene.R
# Load the new expression matrix I made
load("~/repos/T4LS18/data/gene.exprs.Rdata")
```

heatmaps 
========================================================
.


```r
exprs[ which(transcripts == "ESR1"), 1:5]
```

```
          tcga.1    tcga.65  tcga.249   tcga.254  tcga.175
 [1,]   345.4259    37.1002  101.0759     0.0000  247.6399
 [2,]   565.9546   115.9628  633.5632   454.5830  608.1732
 [3,] 11898.5692 11832.1396 5974.6516  2911.1977 4320.4205
 [4,]  5239.9002     0.0000 1671.1465     0.0106  800.0820
 [5,]  1875.6179   339.7354  951.9328   122.0014  632.3651
 [6,]   330.1023    17.8547    0.0000     0.0000   47.6557
 [7,]    16.0000    91.3176    0.0000     0.0000    6.9156
 [8,]     0.0000  2584.0484    0.0000     0.0000    0.0000
 [9,]    25.1429     6.6779    8.1247     0.0000   11.2244
[10,]    27.8710    23.1700    9.1891     0.0000    8.5326
[11,] 16885.8988  3542.8998 8960.3508 10146.2757 4466.4639
[12,]     0.0000     0.0338    0.0000     0.0000    0.0000
[13,]    61.0444     0.0000 1351.2404  1693.4138 1841.9032
[14,]     8.1106     0.0000    0.0000   676.9642  215.9682
[15,]     9.9027     0.0000    0.0000     0.0000    0.0000
[16,]  4572.1621  2816.1374 2057.8057   791.5028 2131.1059
```

```r
gene.exprs[ which(rownames(gene.exprs) == "ESR1"), 1:5]
```

```
  tcga.1  tcga.65 tcga.249 tcga.254 tcga.175 
41861.70 21407.08 21719.08 16795.95 15338.45 
```

```r
gene.exprs[ 1:5, 1:5]
```

```
         tcga.1    tcga.65   tcga.249  tcga.254   tcga.175
A1BG   150.0753   213.3108   299.2189  216.8134   127.1564
A1CF     0.0000     0.5631     0.0000    0.0000     0.0000
A2BP1    0.0000     0.5631     0.8584    1.0646     1.2608
A2LD1   38.3411   283.4798   116.5828  223.7651    98.7723
A2M   4175.7803 13075.3716 16375.9535 6312.9879 24801.4689
```

heatmaps 
========================================================
.


```r
# we are going to plot the expression of the most variable genes..
# first we compute the expression of each gene
evar <- apply(gene.exprs,MARGIN=1,var)
evar[1:5]; length(evar)
```

```
        A1BG         A1CF        A2BP1        A2LD1          A2M 
4.156366e+04 9.154910e-02 1.561851e+00 3.657227e+03 1.029398e+08 
```

```
[1] 20501
```

```r
mostVariable <- gene.exprs[evar>quantile(evar,0.999),]
dim(mostVariable)
```

```
[1]  21 100
```

```r
mostVariable[1:5, 1:5]
```

```
         tcga.1  tcga.65   tcga.249  tcga.254 tcga.175
ACTB  95559.141 63969.60  88495.665 98283.002 91746.70
ADAM6  5144.951 10299.41   7661.301  3804.837 12984.62
C4A   55822.620 11452.70 142885.973  2062.165 23546.19
CD24  19513.256 11109.80  15455.807 30246.451 30946.40
CD74  32897.703 22680.19  16364.417  7731.547 48555.26
```

```r
# so this makes a new matrix that restricted to only  the top .1% of probes.
```
heatmaps 
========================================================
.
We mean center this data to make it more comparable

```r
mcl.mostVar <- t(scale(t(mostVariable)))
```

heatmaps 
========================================================
.


```r
heatmap.2(mcl.mostVar,trace="none",col=greenred(10))
```

![plot of chunk unnamed-chunk-43](M2.L1-figure/unnamed-chunk-43-1.png)


heatmaps 
========================================================

* Ok we explore heatmaps a bit.
* [https://github.com/LeahBriscoe/AdvancedHeatmapTutorial]([https://github.com/LeahBriscoe/AdvancedHeatmapTutorial](https://github.com/LeahBriscoe/AdvancedHeatmapTutorial) A nice tutorial from Leah Briscoe<br>
* [https://www.youtube.com/watch?v=T7_j444LMZs]([https://www.youtube.com/watch?v=T7_j444LMZs](https://www.youtube.com/watch?v=T7_j444LMZs) The YouTube accompanying video<br>> 



```r
# git clone https://github.com/LeahBriscoe/AdvancedHeatmapTutorial.git
```








Probability Distributions
========================================================
- R has functionality to work with many different probability distributions.
- This includes functionality for the probability distribution function (pdf), the cummulat
ive distribution function (cdf), quantiles, simulation etc.
- The different functions have the same form:
- <code>d</code> for the pdf
- <code>c</code> for the cdf
- <code>q</code> for the quantile
- <code>r</code> to generate a random simulation from the distribution.

Probability Distributions
========================================================

```r
? Distributions
```

Probability Distributions: Normal
========================================================

```r
help(norm)
```

Probability Distributions: Normal
========================================================

```r
variates <- rnorm( n=100, mean = 0, sd = 1 ) 
# generates 100 variates from a normal distribution with mean 0 and standard deviation 1
variates[1:10] 
```

```
 [1] -0.86579874 -0.10464350  0.49964635 -1.32562521 -1.56833904
 [6] -0.07448712 -0.03271389 -0.74715165 -0.78441495 -0.55131861
```
Probability Distributions: Normal n=100
========================================================

```r
hist(variates, prob=TRUE) # not ggplot; old style
variates.density <- density(variates)
lines(variates.density)
```

![plot of chunk unnamed-chunk-48](M2.L1-figure/unnamed-chunk-48-1.png)


BIOL480 (c) M Hallett, CB-Concordia
========================================================
![CB-Concordia](MyFigs/sysbiologo.png)

